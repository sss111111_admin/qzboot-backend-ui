export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    viewBtn: true,
    translate: false,
    searchShow: false,
    index: true,
    indexLabel: '序号',
    column: [
        {
            label: '用户名',
            prop: 'username',
            search: true,
        },
        {
            label: '请求IP',
            prop: 'reqIp',
        },
        {
            label: 'IP地区',
            prop: 'ipRegion',
        },
        {
            label: 'token',
            prop: 'token',
        },
        {
            label: '过期时间',
            prop: 'expireTime',
            type: 'datetime',
            overHidden: true
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            overHidden: true
        }
    ]
}
