export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    translate: false,
    searchShow: false,
    index: true,
    indexLabel: '序号',
    column: [
        {
            fixed: true,
            label: '角色名',
            prop: 'roleName',
            search: true,
            maxlength: 20,
            showWordLimit: true,
            rules: [{
                required: true,
                message: '角色名不能为空'
            },
                {
                    min: 2,
                    max: 20,
                    message: '长度在 2 到 20 个字符',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: '备注',
            prop: 'remark',
            type: 'textarea',
            maxlength: 200,
            showWordLimit: true,
            span: 24
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
        }
    ]
}
