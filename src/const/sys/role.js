export const tableOption = {
    tip: false,
    tree: true,
    border: true,
    index: false,
    selection: false,
    viewBtn: true,
    size: 'mini',
    column: [
        {
            label: "角色名称",
            prop: "roleName",
            search: true,
            span: 24,
            rules: [
                {
                    required: true,
                    message: "请输入角色名称",
                    trigger: "blur"
                }
            ]
        },
        {
            label: "备注",
            prop: "remark",
            type: "textarea",
            span: 24
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
            span: 12
        }
    ]
}
