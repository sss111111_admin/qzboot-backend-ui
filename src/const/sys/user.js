export const tableOption = {
    border: true,
    index: false,
    indexLabel: '序号',
     // stripe: true,
    selection: true,
    menuAlign: 'center',
    align: 'center',
    viewBtn: true,
    dialogHeight: 260,
    dialogWidth: '60%',
    size: 'mini',
    translate:false,
    column: [
        {
            fixed: true,
            label: 'ID',
            prop: 'id',
            span: 12,
            width: 180,
            hide: true,
            editDisabled: true,
            addDisplay: false
        },
        {
            fixed: true,
            label: '用户名',
            prop: 'username',
            editDisabled: true,
            search: true,
            span: 12,
            rules: [{
                required: true,
                message: '请输入用户名'
            },
                {
                    min: 3,
                    max: 20,
                    message: '长度在 3 到 20 个字符',
                    trigger: 'blur'
                },
                // { validator: validateUsername, trigger: 'blur' }
            ]
        }, {
            label: '密码',
            prop: 'password',
            type: 'password',
            value: '',
            hide: true,
            editDisplay:false,
            span: 12,
            rules: [{
                min: 6,
                max: 20,
                message: '长度在 6 到 20 个字符',
                trigger: 'blur'
            }]
        }, /*{
            label: '所属部门',
            prop: 'deptId',
            formslot: true,
            slot: true,
            span: 12,
            hide: true,
            rules: [{
                required: true,
                message: '请选择部门',
                trigger: 'blur'
            }]
        },*/ {
            label: '手机号',
            prop: 'phone',
            type: 'phone',
            search: true,
            value: '',
            span: 12,
            rules: [{
                min: 6,
                max: 20,
                message: '长度在 11 个字符',
                trigger: 'blur'
            }]
        }, {
            label: '邮箱',
            prop: 'email',
            type: 'email',
            value: '',
            span: 12,
            rules: [
                {type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur', 'change']}
            ]
        }, {
            label: '角色',
            prop: 'roleIdList',
            formslot: true,
            slot: true,
            overHidden: true,
            hide: true,
            span: 12,
            rules: [{
                required: true,
                message: '请选择角色',
                trigger: 'blur'
            }]
        }, /*  {
            label: '部门',
            prop: 'deptName',
            overHidden: true,
            addVisdiplay: false,
            editVisdiplay: false,
            span: 12,
        },*/ {
            label: '状态',
            prop: 'state',
            type: 'select',
            search: true,
            slot: true,
            span: 12,
            rules: [{
                required: true,
                message: '请选择状态',
                trigger: 'blur'
            }],
            valueDefault: 0,
            dataType: 'number',
            dicUrl: '/sys/dictItem/getByDictCode/COMMON_STATE',
        }, {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
            span: 12
        }]
}
