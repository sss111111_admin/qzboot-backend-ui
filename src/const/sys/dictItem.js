export const tableOption = {
    border: true,
     // stripe: true,
    menuAlign: 'center',
    align: 'center',
    searchSize: 'mini',
    dialogHeight: 200,
    size: 'mini',
    column: [
        {
            label: '字典ID',
            prop: 'dictId',
            display: false,
            hide: true
        },
        {
            label: '字典名称',
            prop: 'dictName',
            span: 24,
            addDisplay: false,
            addDisabled: true,
            editDisabled: true,
        },
        {
            label: '项名称',
            prop: 'itemName',
            span: 12,
            rules: [{
                required: true,
                message: '请输入项名称',
                trigger: 'blur'
            }]
        },
        {
            label: '项值',
            prop: 'itemValue',
            span: 12,
            rules: [{
                required: true,
                message: '请输入项值',
                trigger: 'blur'
            }]
        },
        {
            label: '排序',
            prop: 'sortNum',
            type: 'number',
            valueDefault: 0,
            span: 12,
            rules: [{
                required: true,
                message: '请输入排序',
                trigger: 'blur'
            }]
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
            span: 12
        }
    ]
}
