import dicData from '@/config/dicData'

export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    /*dialogHeight: 260,
    dialogWidth: '60%',*/
    translate: false,
    searchShow: false,
    index: true,
    indexLabel: '序号',
    column: [
        {
            fixed: true,
            label: '用户名',
            prop: 'username',
            maxlength: 20,
            showWordLimit: true,
            search: true,
            editDisabled: true,
            rules: [{
                required: true,
                message: '用户名不能为空'
            },
                {
                    min: 2,
                    max: 20,
                    message: '长度在 2 到 20 个字符',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: '手机号',
            prop: 'phone',
            type: 'phone',
            maxlength: 11,
            showWordLimit: true,
            search: true,
            value: '',
            rules: [{
                required: true,
                min: 11,
                max: 11,
                message: '长度在 11 个字符',
                trigger: 'blur'
            }]
        },
        {
            label: '密码',
            prop: 'newPassword',
            type: 'password',
            maxlength: 20,
            showWordLimit: true,
            hide: true,
            rules: [{
                min: 4,
                max: 20,
                message: '长度在 4 到 20 个字符',
                trigger: 'blur'
            }]
        },
        {
            label: '角色',
            prop: 'roleIdList',
            type: 'select',
            multiple: true,
            hide: true,
            span: 24,
            rules: [
                {
                    required: true,
                    message: '角色不能为空',
                    trigger: 'change'
                }
            ],
            dicData: [],
            props: {
                label: 'roleName',
                value: 'id'
            }
        },
        {
            label: '状态',
            prop: 'state',
            type: 'radio',
            border: true,
            search: true,
            slot: true,
            valueDefault: 0,
            dataType: 'Number',
            dicData: dicData.commonState,
        },
        {
            label: '备注',
            prop: 'remark',
            type: 'textarea',
            span: 24,
            maxlength: 200,
            showWordLimit: true,
            overHidden: true
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
        }
    ]
}
