export const tableOption = {
    border: true,
     // stripe: true,
    menuAlign: 'center',
    align: 'center',
    searchSize: 'mini',
    dialogHeight: 260,
    size: 'mini',
    column: [
        {
            label: '字典代码',
            prop: 'dictCode',
            span: 12,
            search: true,
            editDisabled: true,
            rules: [{
                required: true,
                message: '请输入字典代码',
                trigger: 'blur'
            }]
        }, {
            label: '字典名称',
            prop: 'dictName',
            span: 12,
            search: true,
            rules: [{
                required: true,
                message: '请输入字典名称',
                trigger: 'blur'
            }]
        },
        {
            label: '备注',
            prop: 'remark',
            type: 'textarea',
            span: 24,
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
            span: 12
        }
    ]
}
