export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    viewBtn: true,
    translate: false,
    searchShow: false,
    index: true,
    indexLabel: '序号',
    column: [
        {
            fixed: true,
            label: '类型',
            prop: 'logType',
            type: 'select',
            slot: true,
            search: true,
            dicData: [
                {
                    label: '正常',
                    value: 'NORMAL'
                },
                {
                    label: '异常',
                    value: 'EXCEPTION'
                }
            ]
        },
        {
            label: '内容',
            prop: 'content',
            search: true,
        },
        {
            label: '请求IP',
            prop: 'reqIp',
            hide: true
        },
        {
            label: 'URI',
            prop: 'reqUri',
            span: 24,
            overHidden: true
        },
        {
            label: 'UA',
            prop: 'userAgent',
            span: 24,
            hide: true
        },
        {
            label: '请求类型',
            prop: 'method',
        },
        {
            label: '参数',
            prop: 'params',
            type: 'textarea',
            search:true,
            span: 24,
            overHidden: true
        },
        {
            label: '扩展信息',
            prop: 'extInfo',
            type: 'textarea',
            span: 24,
            hide: true
        },
        {
            label: '耗时',
            prop: 'useTime',
            formatter: function (row, value) {
                return value + 'ms';
            },
            append: 'ms'
        },
        {
            label: '用户',
            prop: 'createByUsername',
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            overHidden: true
        }
    ]
}
