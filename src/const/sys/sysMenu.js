import iconList from "@/config/iconList";
import dicData from '@/config/dicData'


export const tableOption = {
    tip: false,
    expandAll: false,
    expandLevel: 2,
    tree: true,
    border: true,
    rowKey: 'id',
    dialogClickModal: false,
    menuAlign:'left',
    column: [
        {
            label: "类型",
            prop: "menuType",
            type: "radio",
            border: true,
            slot: true,
            editDisabled: true,
            span: 24,
            width: 130,
            valueDefault: 0,
            dicData: [
                {
                    label: "目录",
                    value: 0,
                    type: 'primary'
                },
                {
                    label: "菜单",
                    value: 1,
                    type: 'success'
                },
                {
                    label: "按钮",
                    value: 2,
                    type: 'warning'
                }
            ],
            rules: [
                {
                    required: true,
                    message: "类型不能为空",
                    trigger: "blur"
                }
            ]
        },
        {
            label: "名称",
            prop: "menuName",
            span: 12,
            maxlength: 20,
            showWordLimit: true,
            rules: [
                {
                    required: true,
                    message: "名称不能为空",
                    trigger: "blur"
                }
            ]
        },
        {
            label: "上级",
            prop: "parentId",
            type: "tree",
            // dicUrl: "/sys/menu/select",
            dicData: [],
            hide: true,
            span: 12,
            placeholder: "请选择上级",
            props: {
                label: "menuName",
                value: "id",
                children: 'children'

            },
            rules: [
                {
                    required: true,
                    message: "上级不能为空",
                    trigger: "click"
                }
            ]
        },
        {
            label: "图标",
            prop: "icon",
            type: "icon-select",
            span: 24,
            slot: true,
            iconList: iconList,
            placeholder: '请选择图标',
            display: false,
        },
        {
            label: "路径",
            prop: "path",
            span: 24,
            display: false,
            maxlength: 50,
            showWordLimit: true,
        },
        {
            label: "授权标识",
            prop: "perms",
            span: 24,
            display: false,
            maxlength: 50,
            showWordLimit: true,
        },
        {
            label: "排序",
            prop: "sortNum",
            type: "number",
            span: 24,
            valueDefault: 0
        },
        {
            label: "显隐",
            prop: "hideFlag",
            type: "select",
            dataType: 'number',
            valueDefault: 0,
            span: 24,
            placeholder: '请选择显示或者隐藏',
            slot: true,
            dicData: dicData.hideFlag,
            rules: [
                {
                    required: true,
                    message: "请选择显示或者隐藏",
                    trigger: "click"
                }
            ]
        },
        {
            label: '创建时间',
            prop: 'createTime',
            type: 'datetime',
            editDisabled: true,
            addDisplay: false,
            span: 24
        }
    ]
}
