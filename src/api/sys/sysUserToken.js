import request from '@/router/axios'

export const listPage = (query) => request({
    url: '/sysUserToken/listPage',
    method: 'get',
    params: query
});

export const removeById = (id) => request({
    url: '/sysUserToken/removeById',
    method: 'get',
    params: {id: id}
});
