import request from '@/router/axios';

export const list = (query) => request({
    url: '/sysRole/list',
    method: 'get',
    params: query
});

export const listRoleIdByUserId = (userId) => request({
    url: '/sysRole/listRoleIdByUserId',
    method: 'get',
    params: {userId: userId}
});

export const listPage = (query) => request({
    url: '/sysRole/listPage',
    method: 'get',
    params: query
});

export const save = (data) => request({
    url: '/sysRole/save',
    method: 'post',
    data: data
});

export const updateById = (data) => request({
    url: '/sysRole/updateById',
    method: 'post',
    data: data
});

export const removeById = (id) => request({
    url: '/sysRole/removeById',
    method: 'get',
    params: {id: id}
});

export const listMenuIdByRoleId = (roleId) => request({
    url: '/sysRoleMenu/listMenuIdByRoleId',
    method: 'get',
    params: {roleId: roleId}
});

export const allocateRoleMenu = (data) => request({
    url: '/sysRole/allocateRoleMenu',
    method: 'post',
    data: data
});
