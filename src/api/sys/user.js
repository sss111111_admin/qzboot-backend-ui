import request from '@/router/axios'

export function listPage(query) {
    return request({
        url: '/sys/user/listPage',
        method: 'post',
        data: query
    })
}

export function listUserRole() {
    return request({
        url: '/sys/role/listUserRole',
        method: 'get',
    })
}

export function save(obj) {
    return request({
        url: '/sys/user/save',
        method: 'post',
        data: obj
    })
}

export function getById(id) {
    return request({
        url: '/sys/user/getById/' + id,
        method: 'get'
    })
}

export function deleteByIds(ids) {
    return request({
        url: '/sys/user/delete',
        method: 'post',
        data: ids
    })
}

export function updateById(obj) {
    return request({
        url: '/sys/user/update',
        method: 'post',
        data: obj
    })
}
