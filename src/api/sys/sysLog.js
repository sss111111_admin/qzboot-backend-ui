import request from '@/router/axios'

export const listPage = (query) => request({
    url: '/sysLog/listPage',
    method: 'get',
    params: query
});
