import request from '@/router/axios'

export const listPage = (query) => request({
    url: '/sysUser/listPage',
    method: 'get',
    params: query
});

export const save = (data) => request({
    url: '/sysUser/save',
    method: 'post',
    data: data
});

export const updateById = (data) => request({
    url: '/sysUser/updateById',
    method: 'post',
    data: data
});

export const removeById = (id) => request({
    url: '/sysUser/removeById',
    method: 'get',
    params: {id: id}
});
