import request from '@/router/axios'

export function listPage(query) {
    return request({
        url: '/sys/role/listPage',
        method: 'get',
        params: query
    })
}


export function save(obj) {
    return request({
        url: '/sys/role/save',
        method: 'post',
        data: obj
    })
}

export function getById(id) {
    return request({
        url: '/sys/role/getById/' + id,
        method: 'get'
    })
}

export function deleteByIds(ids) {
    return request({
        url: '/sys/role/delete',
        method: 'post',
        data: ids
    })
}

export function updateById(obj) {
    return request({
        url: '/sys/role/update',
        method: 'post',
        data: obj
    })
}
