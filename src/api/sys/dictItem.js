import request from '@/router/axios'

export function listPage(dictCode, query) {
    return request({
        url: '/sys/dictItem/listPage/' + dictCode,
        method: 'post',
        data: query
    })
}


export function save(obj) {
    return request({
        url: '/sys/dictItem/save',
        method: 'post',
        data: obj
    })
}

export function getById(id) {
    return request({
        url: '/sys/dictItem/getById/' + id,
        method: 'get'
    })
}

export function deleteByIds(ids) {
    return request({
        url: '/sys/dictItem/delete',
        method: 'post',
        data: ids
    })
}

export function updateById(obj) {
    return request({
        url: '/sys/dictItem/update',
        method: 'post',
        data: obj
    })
}
