import request from '@/router/axios'

export const list = () => request({
    url: '/sysMenu/list',
    method: 'get'
});

export const save = (data) => request({
    url: '/sysMenu/save',
    method: 'post',
    data: data
});

export const updateById = (data) => request({
    url: '/sysMenu/updateById',
    method: 'post',
    data: data
});

export const removeById = (id) => request({
    url: '/sysMenu/removeById',
    method: 'get',
    params: {id: id}
});
