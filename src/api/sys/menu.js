import request from '@/router/axios'

export function list(query) {
    return request({
        url: '/sys/menu/list',
        method: 'get',
        params: query
    })
}
export function select() {
    return request({
        url: '/sys/menu/select',
        method: 'get'
    })
}

export function listUserRole() {
    return request({
        url: '/sys/role/listUserRole',
        method: 'get',
    })
}

export function save(obj) {
    return request({
        url: '/sys/menu/save',
        method: 'post',
        data: obj
    })
}

export function getById(id) {
    return request({
        url: '/sys/menu/getById/' + id,
        method: 'get'
    })
}

export function deleteById(id) {
    return request({
        url: '/sys/menu/delete/'+id,
        method: 'post'
    })
}

export function updateById(obj) {
    return request({
        url: '/sys/menu/update',
        method: 'post',
        data: obj
    })
}

export function getDetails(obj) {
    return request({
        url: '/admin/menu/details/' + obj,
        method: 'get'
    })
}
