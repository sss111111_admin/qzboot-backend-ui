import request from '@/router/axios'

export function listPage(query) {
    return request({
        url: '/sys/dict/listPage',
        method: 'post',
        data: query
    })
}


export function save(obj) {
    return request({
        url: '/sys/dict/save',
        method: 'post',
        data: obj
    })
}

export function getById(id) {
    return request({
        url: '/sys/dict/getById/' + id,
        method: 'get'
    })
}

export function deleteByIds(ids) {
    return request({
        url: '/sys/dict/delete',
        method: 'post',
        data: ids
    })
}

export function updateById(obj) {
    return request({
        url: '/sys/dict/update',
        method: 'post',
        data: obj
    })
}
