import request from '@/router/axios';
export const loginByUsername = (username, password, code, randomStr) => request({
    url: '/sys/login',
    method: 'post',
    meta: {
        isToken: false
    },
    data: {
        username,
        password,
        code,
        randomStr: randomStr
    }
})

export const getUserInfo = () => request({
    url: '/sys/getUserInfo',
    method: 'get'
});

export const refeshToken = () => request({
    url: '/user/refesh',
    method: 'post'
})

export const listUserMenu = (type = 0) => request({
    url: '/sys/listUserMenu',
    method: 'get',
    data: {
        type
    }
});

export const getTopMenu = () => request({
    url: '/sys/sysMenu/listTop',
    method: 'get'
});

export const sendLogs = (list) => request({
    url: '/sys/logout',
    method: 'post',
    data: list
})

export const logout = () => request({
    url: '/sys/logout',
    method: 'get'
})

export const changePassword = (data) => request({
    url: '/sys/changePassword',
    method: 'post',
    data: data
})
