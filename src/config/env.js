// 配置编译环境和线上环境之间的切换
let iconfontVersion = ['567566_pwc3oottzol', '1066523_v8rsbcusj5q'];
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`;
const env = process.env;
let baseUrl = process.env.VUE_APP_BASE_URL;
let codeUrl = `${baseUrl}/sys/getCaptcha`;
export {
    baseUrl,
    iconfontUrl,
    iconfontVersion,
    codeUrl,
    env
}
