export default {
    commonState: [
        {
            label: '正常',
            value: 0
        },
        {
            label: '锁定',
            value: 1
        },
    ],
    hideFlag: [
        {
            label: '显示',
            value: 0
        },
        {
            label: '隐藏',
            value: 1
        },
    ],
}
